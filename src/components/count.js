import { Component } from "react";

class Count extends Component {
    constructor(props) {
        super(props);
        this.state= {count: 0}
    }

    onBtnCong = () => {
        console.log("Nút cộng đc bấm");
        let newCount = this.state.count + 1;
        this.setState({
            count: newCount
        })
    }

    onBtntru = () => {
        console.log("Nút trừ đc bấm");
        let newCount = this.state.count - 1;
        this.setState({
            count: newCount
        })
    }

    render() {
        return(
        <div>
            <div className="row">
                <p>Count: {this.state.count}</p>
            </div>
            <div className="row">
                <button onClick={this.onBtnCong}>Cộng thêm</button>
                <button onClick={this.onBtntru}>Giảm xuống</button>
            </div>
        </div>
        )        
    }
}

export default Count;